#!/usr/bin/env bash
# Usage: ./singularity.sh <gpu count> <image name>
set -e; set -o pipefail

GPU_COUNT=${1:-1}
SIMG=${2:-"${PWD}/qmcpack_v3.5.0.simg"}

echo "Downloading S32 Dataset..."
wget -O - https://gitlab.com/NVHCP/ngc-examples/raw/master/qmcpack/v3.5.0/get_S32.sh | bash
INPUT="/host_pwd/NiO-fcc-S32-dmc.xml"

echo "Changing to ./S32_example directory"
cd S32_example

SINGULARITY="singularity run --nv -B ${PWD}:/host_pwd ${SIMG}"
MPIRUN="mpirun -n ${GPU_COUNT}"

echo "Running NiO S32 example in ${SIMG} on ${GPU_COUNT} GPUS..."
${SINGULARITY} ${MPIRUN} qmcpack ${INPUT}
