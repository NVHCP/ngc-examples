#!/bin/bash

ggID='1Kgsm3py0oH4699Cmkg4d5BsZqL_sWoEc'
ggURL='https://drive.google.com/uc?export=download'

if [ ! -f S32_example.tar.gz ]; then
  echo "Downloading S32_example.tar.gz"
  curl -sc /tmp/gcokie "${ggURL}&id=${ggID}" >/dev/null
  getcode="$(awk '/_warning_/ {print $NF}' /tmp/gcokie)"
  cmd='curl -LOJb /tmp/gcokie "${ggURL}&confirm=${getcode}&id=${ggID}"'
  eval $cmd
else
  echo "S32_example.tar.gz already exists, not downloading"
fi

if [ ! -d S32_example.tar.gz ]; then
  tar xf S32_example.tar.gz
  sed -i '/<parameter name="reconfiguration">[[:blank:]]*yes[[:blank:]]*<\/parameter>/d' ./S32_example/NiO-fcc-S32-dmc.xml
  echo "S32_example directory unpacked"
else
  echo "S32_example directory already exists, not unpacking"
fi
