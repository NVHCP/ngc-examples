#!/bin/bash

#SBATCH --nodes=2
#SBATCH --ntasks=16
#SBATCH --ntasks-per-socket=4
#SBATCH --time 00:10:00
set -e; set -o pipefail

# Load required modules
module load singularity
module load openmpi/3.0.1

# Download NiO S32 example input
wget -O - https://gitlab.com/NVHCP/ngc-examples/raw/master/qmcpack/v3.5.0/get_S32.sh | bash
INPUT="/host_pwd/NiO-fcc-S32-dmc.xml"

# Change to ./S32_example directory
cd S32_example

# Singularity alias which to launch qmcpack
SIMG="${SLURM_SUBMIT_DIR}/qmcpack_v3.5.0.simg"
SINGULARITY="$(which singularity) run --nv -B $(pwd):/host_pwd ${SIMG}"

# mpirun alias, setup to launch from the host
MPIRUN="mpirun -np ${SLURM_NTASKS} --map-by ppr:${SLURM_NTASKS_PER_SOCKET}:socket"

# Launch parallel qmcpack
${MPIRUN} ${SINGULARITY} qmcpack ${INPUT}
