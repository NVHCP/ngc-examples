#!/bin/bash

NGPUS=$1

case $NGPUS in
  1)
    GEOM="1 1 1 1"
    ;;
  2)
    GEOM="1 1 1 2"
    ;;
  4)
    GEOM="1 1 1 4"
    ;;
  8)
    GEOM="1 1 2 4"
    ;;
  16)
    GEOM="1 2 2 4"
    ;;
  32)
    GEOM="2 2 2 4"
    ;;
esac

echo $GEOM
