#!/bin/bash

#SBATCH --nodes=2
#SBATCH --time 04:00:00
set -e; set -o pipefail

# Load required modules, if required
module load singularity

# Set cluster/experiment specific variables
readonly gpus_per_node=#
readonly benchmark_dir=#
readonly procs_per_gpu=2
readonly cpus_per_node=${SLURM_CPUS_ON_NODE}
readonly iter=25
readonly pool=100
readonly output_dir="run.$(date +%Y.%m.%d.%H.%M)"
readonly relion_sif="${benchmark_dir}/relion_308.sif"

# Build SIF, if it doesn't exist
if [[ ! -f "${relion_sif}" ]]; then
    singularity build ${relion_sif} docker://nvcr.io/hpc/relion:3.0.8
fi

# Attempt to start MPS server within container if needed
if (( procs_per_gpu > 1 )); then
    srun --ntasks-per-node=1 /bin/bash -c "nvidia-cuda-mps-control -d; sleep infinity" &
fi

# Set Relion 3D classification experiment flags
relion_opts="--gpu \
             --i Particles/shiny_2sets.star \
             --ref emd_2660.map:mrc \
             --firstiter_cc \
             --ini_high 60 \
             --ctf \
             --ctf_corrected_ref \
             --tau2_fudge 4 \
             --K 6 \
             --flatten_solvent \
             --healpix_order 2 \
             --sym C1 \
             --iter ${iter} \
             --particle_diameter 360 \
             --zero_mask \
             --oversampling 1 \
             --offset_range 5 \
             --offset_step 2 \
             --norm \
             --scale \
             --random_seed 0 \
             --pool ${pool} \
             --dont_combine_weights_via_disc \
             --o ${output_dir}"

# Attempt to use as many CPU cores as possible
readonly procs_per_node=$(((gpus_per_node*procs_per_gpu)+1))
readonly tpg_max=6
readonly tpg=$(( cpus_per_node/procs_per_node  ))
readonly threads_per_proc=$(( tpg <= tpg_max ? tpg : tpg_max))
relion_opts+=" --j ${threads_per_proc}"

echo "INFO: Running RELION with:"
echo "  ${SLURM_JOB_NUM_NODES:-$SLURM_NNODES} Nodes"
echo "  ${gpus_per_node} GPUs per node"
echo "  ${procs_per_node} MPI processes per node"
echo "  ${procs_per_gpu} MPI processes per GPU"
echo "  ${threads_per_proc} threads per worker process"

mkdir -p "${benchmark_dir}/${output_dir}"

# WAR for NGC 3.0.8 version
# Create libcufft soname symlink
readonly war_dir="${benchmark_dir}/.relion_308_war"
mkdir -p "${war_dir}"
[[ -f ${war_dir}/libcufft.so.10 ]] || ln -fs /usr/local/cuda/lib64/libcufft.so "${war_dir}"/libcufft.so.10

# Launch parallel relion_refine_mpi experiment
readonly cont_war_dir="/host_pwd/.relion_308_war"
srun --mpi=pmi2 \
     --ntasks-per-node=${procs_per_node} \
     singularity run --nv "-B${benchmark_dir}:/host_pwd" --pwd /host_pwd --env "LD_LIBRARY_PATH=${cont_war_dir}:\${LD_LIBRARY_PATH}" \
     ${relion_sif} \
     relion_refine_mpi \
     ${relion_opts}

# Attempt to exit MPS server
srun --ntasks-per-node=1 /bin/bash -c "echo quit | nvidia-cuda-mps-control"