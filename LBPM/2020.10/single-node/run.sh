#!/bin/bash

export GPU_ARCH="sm70" # Valid options are: sm60, sm70, sm75, sm80
export LBPM_DIR=/usr/local/LBPM_$GPU_ARCH/
mpirun --allow-run-as-root -np 1 -mca pml ucx -x UCX_TLS=sm,cuda_copy -report-bindings --tag-output \
$LBPM_DIR/bin/lbpm_color_simulator input.db
