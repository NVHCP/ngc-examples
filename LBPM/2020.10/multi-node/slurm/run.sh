#!/bin/bash
export BENCHMARK_DIR=$PWD
export GPU_ARCH="sm70" # Valid options are: sm60, sm70, sm75, sm80
module load Singularity
# Build SIF, if it doesn't exist
if [[ ! -f "lbpm.sif" ]]; then
    singularity build lbpm.sif docker://nvcr.io/hpc/LBPM
fi
singularity run -B $BENCHMARK_DIR:/host_pwd --nv lbpm.sif  bash -c 'cp -r /usr/local/LBPM_$GPU_ARCH/example/Sph1896/ /host_pwd/'
srun --mpi=pmi2 --ntasks-per-node=1 singularity run -B $BENCHMARK_DIR:/host_pwd --nv lbpm.sif  bash -c 'cd /host_pwd/Sph1896 && /usr/local/LBPM/bin/GenerateSphereTest input.db'
srun --mpi=pmi2 --ntasks-per-node=1 singularity run -B $BENCHMARK_DIR:/host_pwd --nv lbpm.sif  bash -c 'cd /host_pwd/Sph1896 && /usr/local/LBPM/bin/lbpm_color_simulator input.db'