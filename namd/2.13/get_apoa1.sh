#!/bin/bash

if [ ! -f apoa1.tar.gz ]; then
  echo "Downloading apoa1.tar.gz"
  wget http://www.ks.uiuc.edu/Research/namd/utilities/apoa1.tar.gz
else
  echo "apoa1.tar.gz already exists, not downloading"
fi

if [ ! -d apoa1 ]; then
  tar xf apoa1.tar.gz
  echo "apoa1 example directory unpacked"
else
  echo "apoa1 directory already exists, not unpacking"
fi
