#!/usr/bin/env bash
# Usage: ./singularity.sh <gpu count>
set -e; set -o pipefail

GPU_COUNT=${1:-1}
IMG="nvcr.io/hpc/namd:3.0_alpha3-singlenode"

echo "Downloading APOA1 Dataset..."
wget -O - https://gitlab.com/NVHCP/ngc-examples/raw/master/namd/3.0/get_apoa1.sh | bash
INPUT="/host_pwd/apoa1/apoa1.namd"

SINGULARITY="singularity exec --nv -B $(pwd):/host_pwd docker://${IMG}"
NAMD2="namd3 ${INPUT}"

echo "Running APOA1 example in ${SIMG} on ${GPU_COUNT} GPUS..."
${SINGULARITY} ${NAMD2}
