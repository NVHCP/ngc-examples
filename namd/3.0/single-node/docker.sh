#!/bin/bash
# Usage: ./docker.sh <gpu count>
set -e; set -o pipefail

GPU_COUNT=${1:-1}
IMG="nvcr.io/hpc/namd:3.0_alpha3-singlenode"

echo "Downloading APOA1 Dataset..."
wget -O - https://gitlab.com/NVHCP/ngc-examples/raw/master/namd/2.13/get_apoa1.sh | bash
INPUT="/host_pwd/apoa1/apoa1.namd"

if [ -f /dev/infiniband ]; then
    echo "Enabling Infiniband support"
    IB_DEV="--device=/dev/infiniband --cap-add=IPC_LOCK --net=host"
fi

DOCKER="nvidia-docker run ${IB_DEV} -it --rm -v ${PWD}:/host_pwd ${IMG}"
NAMD2="namd3 ${INPUT}"

echo "Running APOA1 example in ${IMG} on ${GPU_COUNT} GPUS..."
${DOCKER} ${NAMD2}
