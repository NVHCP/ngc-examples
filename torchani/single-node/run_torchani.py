import os
import time

cmd = "python3 vibration_analysis.py"
change_dir = "cd $BENCHMARK_DIR"
os.system(change_dir)
start_time = time.time_ns()
os.system(cmd)
end_time = time.time_ns()
total_time = (end_time - start_time) / 1e9

print(f"Total time : {total_time}")